import React from 'react'
import { Link } from 'gatsby'
import get from 'lodash/get'
import { rhythm } from '../utils/typography'

class PostExcerpt extends React.Component {
  render() {
    const excerpt = this.props.excerpt
    const frontmatter = this.props.frontmatter
    const url = this.props.slug.replace(/\/$/, '.html')
    const title = get(frontmatter, 'title') || url
    const datetime = this.props.publishDate
    const humanDate = new Date(datetime).toLocaleDateString('en-US', {
      day: 'numeric',
      year: 'numeric',
      month: 'long',
    })

    return (
      <article key={url}>
        <h3
          style={{
            marginBottom: rhythm(1 / 4),
          }}
        >
          <Link style={{ boxShadow: 'none' }} to={url}>
            {title}
          </Link>
        </h3>
        <small><time dateTime={datetime}>{humanDate}</time></small>
        <p dangerouslySetInnerHTML={{ __html: excerpt }} />
      </article>
    )
  }
}

export default PostExcerpt
