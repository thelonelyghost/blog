import React from 'react'
import { Link } from 'gatsby'
import get from 'lodash/get'

class TagCloudItem extends React.Component {
  render() {
    const tagName = this.props.name
    return (
      <Link to={`/tag/${tagName}.html`} style={{marginLeft: '1rem'}}>{tagName}</Link>
    )
  }
}

class TagCloud extends React.Component {
  render() {
    const tags = this.props.tags
    if (tags && tags.length) {
      return (
        <p>Tags:
          {' '}{tags.map((tag) => (
              <TagCloudItem key={`tag___${tag}`} name={tag} />
          ))}
        </p>
      )
    }
    return (
      <p>Tags: <em>none</em></p>
    )
  }
}

export default TagCloud
