import React from 'react'
import { Link } from 'gatsby'

import { rhythm, scale } from '../utils/typography'

class Template extends React.Component {
  render() {
    const { location, children } = this.props
    const rootPath = `${__PATH_PREFIX__}/`
    let header
    let footer

    if (location.pathname === rootPath) {
      header = (
        <h1
          style={{
            ...scale(1.5),
            marginBottom: rhythm(1.5),
            marginTop: 0,
          }}
        >
          <Link
            style={{
              boxShadow: 'none',
              textDecoration: 'none',
              color: 'inherit',
            }}
            to={'/'}
          >
            Implements Programmer
          </Link>
        </h1>
      )
    } else {
      header = (
        <h3
          style={{
            fontFamily: 'Montserrat, sans-serif',
            marginTop: 0,
            marginBottom: rhythm(-1),
          }}
        >
          <Link
            style={{
              boxShadow: 'none',
              textDecoration: 'none',
              color: 'inherit',
            }}
            to={'/'}
          >
            Implements Programmer
          </Link>
        </h3>
      )
    }
    footer = (
      <footer
        style={{
          fontSize: '0.8rem',
          color: '#666',
        }}
      >
        Icon made by{' '}
        <a
          style={{
            boxShadow: 'none',
            textDecoration: 'underline',
            color: 'inherit',
          }}
          href={'https://www.freepik.com'}
        >
          Freepik
        </a>
        {' '}from{' '}
        <a
          style={{
            boxShadow: 'none',
            textDecoration: 'none',
            color: 'inherit',
          }}
          href={'https://www.flaticon.com'}
        >
          www.flaticon.com
        </a>
        <br />
        Copyright &copy; 2018, David Alexander
      </footer>
    );
    return (
      <div
        style={{
          marginLeft: 'auto',
          marginRight: 'auto',
          maxWidth: rhythm(24),
          padding: `${rhythm(1.5)} ${rhythm(3 / 4)}`,
        }}
      >
        {header}
        {children}
        {footer}
      </div>
    )
  }
}

export default Template
