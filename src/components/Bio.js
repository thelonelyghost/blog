import React from 'react'

// Import typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

// import profilePic from './profile-pic.jpg'
import profilePic from '../assets/ghost-icon.svg'
import { rhythm } from '../utils/typography'

class Bio extends React.Component {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          marginBottom: rhythm(2.5),
        }}
      >
        <img
          src={profilePic}
          alt={`Happy ghost`}
          style={{
            marginRight: rhythm(1 / 2),
            marginBottom: 0,
            width: rhythm(2),
            height: rhythm(2),
          }}
        />
        <p>
          Written by <strong>David Alexander</strong> who lives and works in the
          Indianapolis area building useful things.{' '}
          <a href="https://twitter.com/thelonelyghost">
            Follow him on Twitter
          </a>
        </p>
      </div>
    )
  }
}

export default Bio
