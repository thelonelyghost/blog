import React from 'react'
import Helmet from 'react-helmet'
import { Link, graphql } from 'gatsby'
import get from 'lodash/get'

import Bio from '../components/Bio'
import Layout from '../components/layout'
import PostExcerpt from '../components/post-excerpt'
import { rhythm, scale } from '../utils/typography'

class BlogTagTemplate extends React.Component {
  render() {
    const siteTitle = get(this.props, 'data.site.siteMetadata.title')
    const siteDescription = get(this.props, 'data.site.siteMetadata.description')
    const posts = get(this.props, 'data.allMarkdownRemark.edges')
    const slug = get(this.props, 'pageContext.slug')

    return (
      <Layout location={this.props.location}>
        <Helmet
          htmlAttributes={{ lang: 'en' }}
          meta={[{ name: 'description', content: siteDescription }]}
          title={siteTitle}
        />
        <h1>Tag: <span style={{fontVariant: 'small-caps'}}>{ slug }</span></h1>
        {posts.map(({ node }) => {
          return (
            <PostExcerpt frontmatter={node.frontmatter} key={node.fields.slug} slug={node.fields.slug} excerpt={node.excerpt} publishDate={node.frontmatter.date} />
          )
        })}
        <hr />
        <Bio />
      </Layout>
    )
  }
}

export default BlogTagTemplate

export const pageQuery = graphql`
  query BlogTagBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    allMarkdownRemark(filter: { frontmatter: { tags: { in: [$slug] } } }, sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "YYYY-MM-DD")
            title
            tags
          }
        }
      }
    }
  }
`
